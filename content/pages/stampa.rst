Stampa
#########

:slug: press
:navbar_sort: 9
:lang: it

Cartella stampa
""""""""""""""""""""""""""""""""""""""


Propaganda
"""""""""""""""""""""""""""""""""""""""

I materiali utili per la diffusione di hackmeeting 2023 sono nell'apposita pagina `Propaganda
<{filename}propaganda.md>`_


Foto
"""""""""""""""""""""""""""""""""""""""


.. image:: images/press/2016-IMG_0578.jpg
    :height: 200px
    :alt: La bacheca dei seminari dell'hackmeeting 2016, a Pisa
    :target: https://hackmeeting.org/hackit16/images/photos/IMG_0578.jpg

.. image:: images/press/2016-IMG_0581.jpg
    :height: 200px
    :alt: Sessione di elettronica digitale
    :target: https://hackmeeting.org/hackit16/images/photos/IMG_0581.jpg

.. image:: images/press/2016-IMG_0584.jpg
    :height: 200px
    :alt: Programmazione
    :target: https://hackmeeting.org/hackit16/images/photos/IMG_0584.jpg

.. image:: images/press/2016-IMG_0586.jpg
    :height: 200px
    :alt: Computer
    :target: https://hackmeeting.org/hackit16/images/photos/IMG_0586.jpg

.. image:: images/press/2016-IMG_0589.jpg
    :height: 200px
    :alt: Il LAN party: un posto dove sperimentare insieme
    :target: https://hackmeeting.org/hackit16/images/photos/IMG_0589.jpg

.. image:: images/press/2016-IMG_0574.jpg
    :height: 200px
    :alt: Un hack su Emilio: il famoso robottino degli anni '90 è stato riprogrammato
    :target: https://hackmeeting.org/hackit16/images/photos/IMG_0574.jpg


Comunicati stampa
"""""""""""""""""""""""""""""""""""""""




Comunicato Stampa
=================================

| HACKMEETING
| 7-10 settembre 2023
| Centro Sociale Occupato Autogestito Cartella
| Via Prima Quarnaro, Gallico Marina (Reggio Calabria) 
|

Quattro giorni di seminari, giochi, dibattiti. Torna Hackmeeting,
l'annuale incontro di chi si definisce hacker, la cui ventiseiesima
edizione si svolge al CSOA Angelina Cartella di Reggio Calabria
Gallico, dal 7 al 10 settembre. 
Hackmeeting è per sua natura nomade, si sposta di città in città per
raccontare e ascoltare esperienze come quella del centro sociale
Cartella, che da oltre vent'anni costituisce una cellula anomala
di autogestione. In un territorio incendiario ed aspro, in cui
delega e verticalizzazione sono la norma, il Cartella si rispecchia
nella comunità hacker per l’attitudine a “condividere saperi senza
fondare poteri”.

Una formula consolidata che, dal 1998, ha accompagnato e previsto
tutte le fasi della rete: dalla fine della neutralità del web, alla
trasformazione della blockchain e delle intelligenze artificiali
fino ad arrivare alle insidie degli smartphone.

Un momento di condivisione di conoscenze, tanto teoriche quanto
pratiche, per rimodellare il reale mettendoci le mani dentro,
in inglese "hacking". Dal cucirsi la borsa per il computer a
realizzare gioielli con i rifiuti elettronici, Hackmeeting non
è solo rifugio di coder e smanettoni, ma anche laboratorio per
le più disparate forme di espressione. Pratiche di apprendimento
collettivo per analizzare insieme le tecnologie che utilizziamo
quotidianamente: come cambiano, come incidono sulle nostre vite
reali e virtuali, al fine di usarle consapevolmente.

Tanti i seminari gratuiti e aperti a tutte e tutti: oltre a
immergersi nelle tecnologie conviviali s’imparerà a realizzare
una macchina per serigrafare, così come a riparare un motore termico.
Tra un workshop e un dj set, verranno condivise riflessioni per
comprendere come le Big Tech usino i dati personali per un modello
di business basato sulla manipolazione pervasiva dell’attenzione,
della salute mentale e delle emozioni.

