Title: How to get there
slug: come-arrivare
lang: en

Hackmeeting 2023 will be held at [CSOA Angelina Cartella](http://www.csoacartella.org/) - [Via Quarnaro 1, Gallico Marina, Reggio Calabria](https://www.openstreetmap.org/node/10557820765#map=16/38.1679/15.6470)

<iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox=15.644788742065431%2C38.1666131432927%2C15.648887157440187%2C38.16919426711531&amp;layer=mapnik&amp;marker=38.16790371662848%2C15.646837949752808" style="border: 1px solid black"></iframe><br/><small><a href="https://www.openstreetmap.org/?mlat=38.16790&amp;mlon=15.64684#map=18/38.16790/15.64684" target='_blank'>View map</a></small>


## By train

TBD

## By autobus

TBD

## By car

TBD


## By Airplain

### From Airport of Reggio Calabria

TBD

### From Airport of Lamezia

