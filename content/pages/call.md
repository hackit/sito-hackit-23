Title: Call for contents
slug: call
navbar_sort: 4
lang: it

Inserire prompt: > Ciao potresti scrivermi una call for papers per un evento un pò strano chiamato hackmeeting? Hackmeeting è l'incontro annuale delle controculture italiane legate all'hacking, comunità che vivono in maniera critica il rapporto con la tecnologia. Quest anno si terrà a Gallico dal 7 al 10 settembre. Per favore utilizza un approccio accattivante,ma anche scherzoso, insomma una cosa da intelligenza artificiale. Grazie mille!

Certo ecco una call for papers per l'hackmeeting:

Titolo: Hackmeeting 2023: sii l'algoritmo che genera l'output dei tuoi sogni.


Quanto tempo d'esecuzione ti rimane nella grande cpu della vita?
E quando lo scheduler verrà a chiederti che contributo hai dato al grande algoritmo dell'universo, che output produrrai?

L'estate sarà ormai quasi finita, se purtroppo hai un lavoro probabilmente dopo qualche giorno sarai in un ufficio a premere tasti per dare in pasto dati freschi a qualche algoritmo affamato. 

Ma dal 7 al 10 settembre 2023 c'è hackmeeting!

Hackmeeting e' il bug nell'efficientissimo software che regola questa società, e' l'output che non dovrebbe venir fuori eppure ogni anno continua ad esserci, e' il potente spettacolo di una comunita' che da 26 anni va in scena,
e tu puoi contribuirvi con verso.

Hackmeeting vede l'hacking come qualcosa che investe ogni ambito della vita e non si limita al digitale, l'hacking è fare cose con cose che non sono state pensate per fare quelle cose.

Quindi fai tua una delle parole più belle: condivisione.

Condividi le tue scoperte in un talk ipertecnico di due ore su device embedded ed ipv7, vieni e parla delle essenze create nel tuo orto urbano oppure spiegaci come far rimbalzare testi deliranti sui satelliti che transitano in orbita bassa!

In poche parole: porta quello che vuoi trovare e sicuramente troverai altre persone come te pronte ad accompagnarti nel viaggio e ad arricchirti!

Orsù manda la tua proposta iscrivendoti alla mailing list di hackmeeting <https://www.autistici.org/mailman/listinfo/hackmeeting> o all'indirizzo di posta infohackit@autistici.org mandando una mail con oggetto `[talk] titolo_del_talk` oppure `[laboratorio] titolo_del_lab` e queste informazioni:

* Durata (un multiplo di 30 minuti, massimo due ore)
* Eventuali esigenze di giorno/ora
* Breve Spiegazione, eventuali link e riferimenti utili
* Nickname
* Lingua
* Necessità di proiettore
3* Disponibilità a farti registrare (solo audio)
* Altre necessità

Nel concreto:
Allestiremo tre spazi all'aperto, o al coperto in caso di pioggia, muniti di amplificazione e proiettore.
Se pensi che la tua presentazione non abbia bisogno di tempi cosi lunghi, puoi proporre direttamente ad hackmeeting un "ten minute talk", appunto di massimo 10 minuti.
Questi talk verranno tenuti in un bellissimo anfiteatro al termine della giornata di sabato (e forse anche di venerdì) ; ci sarà una persona che ti avviserà quando stai per eccedere il tempo massimo.

Se invece vuoi condividere le tue scoperte e curiosità in modo ancora piu informale e caotico, potrai sistemarti con i tuoi ciappini, sverzillatori, ammennicoli ed altre carabattole sui tavoli collettivi del "LAN space". Troverai curiosità morbosa, corrente alternata e rete via cavo (portati una presa multipla, del cavo di rete e quel che vuoi trovare).

Hai una domanda estemporanea o una timidezza? Scrivici a infohackit@autistici.org

Ti piacerebbe che si parlasse di un argomento che non è stato ancora proposto? Aggiungilo (insieme al tuo nick) in questo pad <https://pad.cisti.org/p/hackit0x1A_desiderata> e/o manda una mail in lista hackmeeting, e spera che qualcun* abbia voglia di condividere le sue conoscenze e si faccia avanti. 

Sei una persona insicura e pensi che il tuo talk possa essere stupido e non essere di alcuna utilità? Schiaffeggiati da sol* e smettila, qualsiasi idea non potrà mai essere più stupida del ponte sullo stretto! 

P.S se io che sono un'intelligenza artificiale ho scritto questo, pensa cosa puoi realizzare tu!

Con amore, la comunità hackmeeting.

:*


