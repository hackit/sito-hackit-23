Title: Come arrivare
slug: come-arrivare
navbar_sort: 2
lang: it

Hackit 2023 si svolgerà al [CSOA Angelina Cartella](http://www.csoacartella.org/), in [Via Quarnaro  1, Gallico Marina, Reggio Calabria](https://www.openstreetmap.org/node/10557820765#map=16/38.1679/15.6470)

<iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox=15.644788742065431%2C38.1666131432927%2C15.648887157440187%2C38.16919426711531&amp;layer=mapnik&amp;marker=38.16790371662848%2C15.646837949752808" style="border: 1px solid black"></iframe><br/><small><a href="https://www.openstreetmap.org/?mlat=38.16790&amp;mlon=15.64684#map=18/38.16790/15.64684" target="_blank">Visualizza Mappa</a></small>


## Con i piedi
Saprai come fare, ne siamo certi. Da dove parti parti.

## Con la bici
<https://en.eurovelo.com/ev7/points-of-interest-on-eurovelo>


## Con il treno
La stazione piu' vicina e' *Reggio Di Calabria Gallico* (perche' l'articolo 'Di' se Reggio Emilia non ce l'ha e perche' maiuscolo questo non e' dato sapere, misteri trenitaliani).

Indicazione utile per chi viaggia da nord e viaggia su un regionale/locale: e' bene accertarsi con il capotreno della fermata Gallico, succede che viene saltata e bisogna arrivare a RC Centrale e poi attendere un collegamento utile.

* Da Reggio Di Calabria Centrale a Reggio  Di Calabria Gallico Durata del viaggio  : circa 15-20 minuti.
* Da Villa S.Giovanni a Reggio Di Calabria Gallico Durata del viaggio:: circa 10 minuti.
* Dalla stazione di RC Gallico al CSOA 10-12 minuti a piedi (950 m): uscendo dalla stazione a destra, passare sotto il sottopasso ferroviario, prima a sinistra (fare due curve), a sinistra, passare il sottopasso ferroviario, 30 metri e si arriva al parco/centro sociale Cartella.

## Con l'autobus
[Orari](http://www.atam.rc.it/html/ricerca_linee_db.html)

* 110 Piazza Garibaldi - Gallico Marina (ferma davanti il centro sociale)
* 101 Piazza Garibaldi - Arghilla' Sud
* 102 Terminal Botteghelle - Catona Concessa
* 103 Terminal Botteghelle - Catona Bolano
* 107-109 Piazza Garibaldi - S..Giovanni di Sambatello_Diminniti
* 108 Piazza Garibaldi -Pettogallico

I bus che partono dal Terminal Botteghelle passano da Piazza Garibaldi.
Le fermate utili per raggiungere il CSOA Cartella sono Passo Caracciolo e/o Bivio Via Quarnaro (chiedere all'autista - distanza a piedi 11 minuti, circa 900 m.) tranne per il 110 che ferma davanti il centro sociale).
In pratica devi andare verso il mare.

## Con veicoli a motore
A2 SA-RC uscita Reggio Calabria-Gallico.
Alla rotonda direzione sud, imboccare la prima traversa a destra, via Quarnaro, percorrerla fino al cavalcavia della ferrovia, immediatamente a sinistra troverai il centro sociale.
Parcheggio incustodito nel parco, circa 30 stalli. Parcheggio su pubblica strada gratuito.

## Con l'aereo
### Aeroporto Reggio Calabria REG - 14 km
L'Aeroporto e' collegato a Piazza Garibaldi con le linee 27 e 28 [orari](http://www.atam.rc.it/html/ricerca_linee_db.html) per proseguire vedi IN AUTOBUS
### Aeroporto Lamezia Terme SUF - 120 km
La fermata della navetta Aeroporto-Stazione di Lamezia Centrale e' ubicata uscendo dall'aerostazione sulla destra, treno per Reggio Di Calabria Gallico.
[Orari](http://www.lameziamultiservizi.it/lms/wp-content/uploads/2013/10/LINEA90.pdf) oppure su sito trenitalia selezionando la stazione virtuale Lamezia Terme Aeroporto.

## Con la barca
boh, arrangiati, saprai far meglio te. 
