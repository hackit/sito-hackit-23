Title: Accessibilità
slug: accessibility
navbar_sort: 2

Hackmeeting è uno spazio e una comunità autogestita: tutto quello che 
troverai è autocostruito con povertà di mezzi, generalmente recuperando 
strutture abbandonate e riadeguandole il più possibile ai nostri bisogni.
Abbiamo cercato di rendere Hackmeeting più accessibile che potevamo, ma 
ci sono sicuramente ancora tante cose che non ci sono riuscite e altre 
che dovremmo migliorare. Abbi pazienza e aiutaci segnalandoci cosa non 
va, in modo da fare meglio il prossimo anno!

## Lo spazio

Hackit 2023 si svolgerà al CSOA Angelina Cartella, Via Quarnaro  1, Gallico Marina, Reggio Calabria


## Aule seminari

Le aule dei seminari saranno 3

## Cucina

La cucina e lo spazio per mangiare si trovano nella zona a sinistra del parco.
Si accede tramite scale oppure anche con una rampa.

## Dormire

Lo spazio per dormire è purtroppo limitato: verrà allestito solo uno spazio tende in giardino.
Non ci sono letti a disposizione o stanze singole o palestre.

## Bagni

E’ presente un bagno per persone con ridotta mobilità, con doccia. Si trova nei pressi del lan space, più
precisamente vicino alla cucina.

Oltre a questo, all’esterno saranno installati bagni chimici, che però non sono accessibili.

Tutti i bagni sono gender neutral.
Le docce verranno costruite nei giorni precedenti Hackmeeting: saranno
spartane e difficilmente consentiranno l’utilizzo a persone con ridotta mobilità.

## Modalità aereo

Hackmeeting a volte può essere faticoso: se hai bisogno di prenderti un 
attimo di pausa abbiamo allestito una stanza dove puoi startene per i 
fatti tuoi senza che i tuoi spazi vitali vengano disturbati.

## Clima

L'hackmeeting si svolge tutto all'aperto, costruiremo spazi all’esterno ombreggiati
e sono presenti diversi maestosi alberi.

## Linguaggi

Non siamo ancora in grado di garantire un servizio di traduzioni in 
simultanea, ma Hackmeeting è frequentato da persone che parlano diverse 
lingue, compresa la LIS. Se hai bisogno, o se vuoi offrirti per 
tradurre, rivolgiti all'info point.

## Cani da assistenza

Se hai un cane che ti accompagna è il benvenuto. Troverà acqua e 
probabilmente altri cani con cui giocare. Se quest'ultima cosa dovesse 
essere un problema segnalacelo all'info point all'ingresso e cercheremo 
di limitare l'eventuale invadenza degli altri cani.

## Altre info

Nello spazio saranno presenti un po' ovunque prese per ricaricare 
qualunque dispositivo.
La musica ad alto volume sarà presente in modo continuativo solo venerdì 
sera e in un luogo lontano dagli altri spazi comuni.
Per qualsiasi altra informazione o necessità che non stiamo 
considerando, puoi rivolgerti all'info point all'ingresso.

Torna alla pagina delle [informazioni principali]({filename}info.md)
