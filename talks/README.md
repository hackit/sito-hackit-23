Per aggiungere un talk, copia la directory `_talk_example` in una nuova
directory che contenga **solo caratteri alfabetici**, quindi
cambia il file ``meta.yaml`` a tuo piacimento.
Usa UTF-8 o morirai.
**NOTA BENE**: in realta' abbiamo usato il calendario di nextcloud con uno script magico che autopusha
